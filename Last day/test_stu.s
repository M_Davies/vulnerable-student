#SEC204 - Vulnerable Student
#   INSTRUCTIONS GO HERE    
.section .data
average:
    .string "Your average score is %d \n"
question:
	.string "What is your name? \n"

.section .text
.globl _start
_start:
#new
		pushl	%eax
		call	srand
		pushl   %ebp #pushes base pointer
        movl    %esp, %ebp #sets stack pointer to base
        subl    $32, %esp  #creates this 16 byte buffer to put stuff in
        movl    $0, -4(%ebp) #empties where the random number is going
        movl    $0, %eax #clears eax for getRand to write to it
		# maybe clearing the eax was not necessary
        call    getRand
		movl    %edx, -4(%ebp) #stores the random number on the stack
		movl    $0, -8(%ebp)
		movl    $0, %eax
        call    getRand
		movl    %edx, -8(%ebp) #stores the random number on the stack
		#averaging
		mov		-8(%ebp), %ebx # put num 2 in ebx
		mov		-4(%ebp), %eax # put num 1 in eax
		add		%ebx, %eax
		mov		$2, %ecx	#put 2 in ecx for the divide
		mov		$0, %edx
		div		%ecx		#divides eax by ecx (2)
		movl    %eax, -4(%ebp) #stores average back on the stack
		#get name
		pushl	$question
		call	printf
		add		$4, %esp 
		lea		(%esp), %eax	#puts stack address into the eax
		movl 	%eax, (%esp)
		call	gets
		add		$28, %esp #moves from current esp to the average
		push	$average
		call	printf
		pushl	$0 #push successfull exit code to the stack
		call	exit
		
getRand:
   # pushl %ebp
    #movl %esp, %ebp

    call rand
    mov $0, %edx
    mov $100, %ebx
    div %ebx
    add $1, %ebx
    ret