#SEC204 - Vulnerable Student
.section .data
score:
	.string "You scored %d \n"
question:
	.string "What is your name? \n"
namefmt:
	.string "%s"
answer:
	.string "Hello %s \n"
average:
    	.string "Your average score is %d \n"
name:
	.long 16

.section .text
.globl _start
_start:
    pushl %eax			#set up stack	
    call srand			#seeds random number generation

	mov $question, %eax	#moves question to eax
	pushl %eax
	call printf			#prints out
	addl $8, %esp		#clean stack 
	call getName		#gets the name from the user

	call getRand		#gets first random number

	mov %edx, %ebp		#store the first random number in ebp to clear the edx for the next number
 	push %edx			#push number to the stack for printing	
	pushl $score		
    call printf
    addl $12, %esp		#clean stack

    call getRand		#get second random number

    addl %edx, %ebp		#add the first random to the second for average calculation
    push %edx			#push number to the stack for printing
    pushl $score
    call printf
    addl $12, %esp		#clean stack

#Calculate Average
    xor %edx, %edx		#clear edx for div again
    mov %ebp, %eax		#sum of two random numbers...
    mov $2, %ecx		#...divided by 2
    div %ecx
    push %eax			#average is now in eax
    pushl $average		#push to stack for printing
    call printf
    addl $12, %esp		#clean stack

	pushl $0			#push sucessfull exit code to the stack
	call exit			#close program

getRand:
    call rand			#get random number
    mov $0, %edx		#div requires edx to be empty for the remainder
    mov $100, %ebx		#divide by upper bound to get the range between 0-100
    div %ebx
    add $1, %ebx		#add 1 to include upper bound
    ret


getName:
	pushl %ebp			#save old frame pointer
	movl %esp, %ebp		#create new one
	pushl $name			#push address for name to be stored
	pushl $namefmt		#give scanf address of format string
	call scanf
	addl $8, %esp		#clean stack

	pushl $name			#pass input to printf by value
	pushl $answer		#pass address of output format
	call printf
	addl $8, %esp		#clean stack

	movl $0, %eax		#function finished sucessfully
	movl %ebp, %esp		#release used memory
	popl %ebp			#restore old pointer
	ret
