﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VulnStudent
{
    class Program
    {
        static void Main(string[] args)
        {
            //Take name
            Console.WriteLine("What is your name?");
            String getName = Console.ReadLine();

            //Start Generator
            Random rng = new Random();

            //Generate Numbers
            int score1 = rng.Next(1, 100);
            int score2 = rng.Next(1, 100);

            //Calc Average
            Console.WriteLine(getName + ", your score is ");
            Console.Write((score1 + score2) / 2);
            Console.ReadLine();
        }
    }
}
