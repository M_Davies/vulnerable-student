#SEC204 - Vulnerable Student
.section .data
score:
	.string "You scored a %d \n"
average:
    .string "Your average score is %d \n"
question:
	.string "What is your name? \n"
namefmt:
	.string "%s"
answer:
	.string "Hello %s \n"
name:
	.short 1

.section .text
.globl _start
_start:
#new
		pushl   %ebp #pushes base pointer
        movl    %esp, %ebp #sets stack pointer to base
        subl    $16, %esp  #creates this 16 byte buffer to put stuff in
        movl    $0, -4(%ebp) #empties where the random number is going
        movl    $0, %eax #clears eax for getRand to write to it
		# maybe clearing the eax was not necessary
        call    getRand
		movl    %edx, -4(%ebp) #stores the random number on the stack
#		pushl	%edx
#		pushl	$score
#		call	printf
#		addl	$8, %esp #we remove the 2 things we pushed
		movl    $0, -8(%ebp)
		movl    $0, %eax
        call    getRand
		movl    %edx, -8(%ebp) #stores the random number on the stack
#		pushl	%edx
#		pushl	$score
#		call	printf
		#averaging
		mov		-8(%ebp), %ebx # put num 2 in ebx
		mov		-4(%ebp), %eax # put num 1 in eax
		add		%ebx, %eax
		mov		$2, %ecx	#put 2 in ecx for the divide
		div		%ecx		#divides eax by ecx (2)
		movl    %edx, -4(%ebp) #stores average back on the stack
		#get name
		pushl	$name		#push address for name to be stored
		pushl	$namefmt		#give scanf address of format string
		call	scanf
		addl	$4, %esp
		pop		$eax			#this should be the name
		#addl	$8, %esp		#removes the name/formating stuff
		#mov	-8(%ebp), $name

		push	$name		#pass input to printf by value
		pushl 	$answer		#pass address of output format
		call 	printf
		addl 	$8, %esp
		

		pushl	$0 #push successfull exit code to the stack
		call	exit
		

#end new
#    pushl %eax
 #   call srand
#
 #   call getRand
#
 #   mov %edx, %ebp
  #  push %edx
   # pushl $score
    #call printf
    #addl $12, %esp
#
 #   call getRand
#
 #   addl %edx, %ebp
  #  push %edx
   # pushl $score
    #call printf
#    addl $12, %esp

#put avg on stack
#	movl %edx, -32(%esp) #SHOVE IT TO THE STACK SO IT CAN BE OVERWRITTEN


#getname here to overwrite score value
#	mov $question, %eax	#moves question to eax
#	pushl %eax
#	call printf		#prints out
#	addl $8, %esp		#clean stack
#	call getName		#gets the name from the user

#average
#
 #   xor %edx, %edx
#    mov %ebp, %eax
 #   mov $2, %ecx
  #  div %ecx
	#mov -32(%esp), %eax # this might not work
#    push %eax
 #   pushl $average
  #  call printf
   # addl $12, %esp

#	pushl $0		#push successfull exit code to the stack
#	call exit

getRand:
   # pushl %ebp
    #movl %esp, %ebp

    call rand
    mov $0, %edx
    mov $100, %ebx
    div %ebx
    add $1, %ebx
    ret


getName:
	pushl %ebp		#save old frame pointer
	movl %esp, %ebp		#create new one
	pushl $name		#push address for name to be stored
	pushl $namefmt		#give scanf address of format string
	call scanf
	addl $8, %esp		#clean stack

	push $name		#pass input to printf by value
	pushl $answer		#pass address of output format
	call printf
	addl $8, %esp		#clean stack

	movl $0, %eax		#program finished successfully
	movl %ebp, %esp		#release used memory
	popl %ebp		#restore old pointer
	ret
