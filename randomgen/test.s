.section .data
line1: .string "The score is %d \n"

.section .text
.globl _start
_start:

    pushl %eax
    call srand #seeds the random number generation
   # addl  $4, %esp seeming not all that useful
    pushl $line1

    call rand
    nop
    nop
    mov $0, %edx #div requires edx to be empty as thats were it stores the remainder
    mov $100, %ebx #this is the value we want to divide by, as we want the value between 0 and 100 we choose 100 here
    div %ebx #divide the eax by the ebx(100). div stores the result in eax and the remainder in the edx
    #and $100, %eax
    nop
    nop
    push %edx #push the remainder of the division (random/100) to the stack, allowing for later printing
    nop
    nop
    pushl $line1 #push the string thats going to format the int to the stack
    nop
    nop
    call printf #prints the string and grabs the pushed edx value from the earlier division for the %d value
    addl $8, %esp #cleans the stack a little, i dont think this is really cleaning it fulling since we shove a string and an int there thats gotta be more than 4 bytes

    pushl $0
    call exit
