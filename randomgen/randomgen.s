.section .data
line1: .string "The score is %d \n"

.section .text
.globl _start
_start:

    pushl %eax
    call srand
    pushl $line1

    call rand
    mov $0, %edx
    mov $100, %ebx 
    div %ebx 
    push %edx 

    pushl $line1
    call printf
    addl $8, %esp 

    pushl $0
    call exit